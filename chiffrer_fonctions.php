<?php
/**
 * Fonctions utiles au plugin Chiffrer
 *
 * @plugin     Chiffrer
 * @copyright  2021
 * @author     g0uZ
 * @licence    GNU/GPL
 * @package    SPIP\Chiffrer\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Créé la clé de chiffrement
 */	
function initialiser_cle(){
	$fichier_cles = _DIR_ETC."cles.php";
	if ( ! file_exists($fichier_cles) ){
		$GLOBALS['cle_secrete'] = openssl_random_pseudo_bytes(16);
		ecrire_fichier($fichier_cles, "<?php\n\n\$GLOBALS['cle_secrete'] = base64_decode('".base64_encode($GLOBALS['cle_secrete'])."');\n");
	}
}

/**
 * Restaure la clé de chiffrement
 *
 * @param int $id_auteur
 *     Identifiant de l'auteur
 * @param string $pass
 *     Mot de passe de l'auteur en clair
 * @return void
 */	
function restaurer_cle($id_auteur, $pass){
	$fichier_cles = _DIR_ETC."cles.php";
	if ( ! file_exists($fichier_cles) ){
		if ( $cle_secrete_bkp = lire_config("chiffrer_cle_sauvegarde_id_auteur_$id_auteur") ){
			$cle_secrete_potentielle = base64_decode(dechiffrer($cle_secrete_bkp, $pass));
			$pass_poivre = hash_hmac("sha256", $pass, $cle_secrete_potentielle);
			$pass_db = sql_getfetsel('pass', 'spip_auteurs', 'id_auteur='.sql_quote($id_auteur));			
			if ( password_verify($pass_poivre, $pass_db) ){
				spip_log("restauration de la cle secrete par id_auteur $id_auteur", _LOG_INFO_IMPORTANTE);
				$GLOBALS['cle_secrete'] = $cle_secrete_potentielle;
				ecrire_fichier($fichier_cles, "<?php\n\n\$GLOBALS['cle_secrete'] = base64_decode('".base64_encode($GLOBALS['cle_secrete'])."');\n");
			}
		}
	}
}

/**
 * Chiffrer
 *
 * @param string $clair
 *     Texte à chiffrer
 * @param string $cle
 *     Clé à utiliser
 * @param string $cipher
 *     Cipersuite à utiliser
 * @return string
 *	   Texte chiffré
 */	
function chiffrer($clair, $cle=false, $cipher="AES-128-CBC"){
	$cle = ( $cle ) ? $cle : $GLOBALS['cle_secrete'];
	$ivlen = openssl_cipher_iv_length($cipher);
	$iv = openssl_random_pseudo_bytes($ivlen);
	$chiffre_raw = openssl_encrypt($clair, $cipher, $cle, $options=OPENSSL_RAW_DATA, $iv);
	$hmac = hash_hmac('sha256', $chiffre_raw, $cle, $as_binary=true);
	$chiffre = base64_encode( $iv.$hmac.$chiffre_raw );
	spip_log("chiffrer($clair)=$chiffre", _LOG_DEBUG);
	return $chiffre;
}

/**
 * Dechiffrer
 *
 * @param string $chiffre
 *     Texte à déchiffrer
 * @param string $cle
 *     Clé à utiliser
 * @param string $cipher
 *     Cipersuite à utiliser 
 * @return string
 *	   Texte déchiffré 
 */	
function dechiffrer($chiffre, $cle=false, $cipher="AES-128-CBC"){
	$cle = ( $cle ) ? $cle : $GLOBALS['cle_secrete'];
	$c = base64_decode($chiffre);
	$ivlen = openssl_cipher_iv_length($cipher);
	$iv = substr($c, 0, $ivlen);
	$hmac = substr($c, $ivlen, $sha2len=32);
	$chiffre_raw = substr($c, $ivlen+$sha2len);
	$clair = openssl_decrypt($chiffre_raw, $cipher, $cle, $options=OPENSSL_RAW_DATA, $iv);
	spip_log("dechiffrer($chiffre)=$clair", _LOG_DEBUG);	
	$calcmac = hash_hmac('sha256', $chiffre_raw, $cle, $as_binary=true);
	if ( hash_equals($hmac, $calcmac) ){ // timing attack safe comparison	
		return $clair;
	}
}

