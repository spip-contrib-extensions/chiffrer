## Plugin chiffrer : vous prendrez bien du poivre avec votre sel ? ##

### Fonctionnalitées ###

- supprime le hash réalisé coté client en Javascript et son traitement coté serveur en PHP
- génère un poivre (une clé secréte stockée sur le disque dans config/cles.php)
- utilise les fonctions PHP adéquates pour le caclul et la vérification du hash du mot de passe avec password_hash() (en temps constant)
- met à jour le hash du mdp de l'utilisateur lors du login
- autorise le login avec ce type hash tout en restant compatible avec les anciens algorithmes md5 & sha
- sauvegarde le poivre de façon sécurisée dans la DB lors du login des webmestre
- restore le poivre depuis la DB lors du login d'un webmestre si fichier de config absent
- expose une API basique pour chiffrer()/dechiffrer() des données


### Pourquoi ? ###

SPIP ne stocke pas les mots de passe des utilisateurs tels quels dans la base de données. Il les protègent a l'aide d'une fonction de hachage https://fr.wikipedia.org/wiki/Fonction_de_hachage. Le mot de passe protégé : le hash, est salé https://fr.wikipedia.org/wiki/Salage_(cryptographie) afin d'éviter certaines attaques permettant de casser cette protection.

Historiquement, le web fonctionnait avec HTTP,c'est à dire sans mécanisme de chiffrement protégeant les données transportées. Afin de protéger le mot de passe de l'utilisateur lors de son transit entre le navigateur web et le serveur HTTP ; SPIP hache de le mot de passe de l'utilisateur dès sa saisie dans le navigateur (a l'aide de Javascript).

Ce mécanisme est vulnérable à une attaque de type Pass The Hash https://en.wikipedia.org/wiki/Pass_the_hash consistant a envoyer le hash du mot de passe au serveur. Ainsi dans le cas d'une fuite de la base de données (sauvegarde perdue, injection SQL en lecture), l'attaquant est en mesure de s'authentifier avec le hash sans avoir besoin de connaitre le mot de passe correspondant.

Ce plugin implémente un mécanisme de sécurité supplémentaire au hash et au sel : le poivre https://en.wikipedia.org/wiki/Pepper_(cryptography). Le poivre est stocké a un endroit différent des hash et sels, dans notre cas : config/cles.php.

Il n'est ainsi plus possible de réaliser une attaque de type Pass The Hash suite à la récupération des hash et sels des mots de passes des auteurs contenus dans la base de données. De la même manière il est désormais impossible d'espérer casser un hash de mot de passe sans disposer des trois éléments hash/sel/poivre et quand bien même : la sécurité des nouvelles fonctions de hachage utilisées (bcrypt ou Argon2) est trop grande.


### ATTENTION ###

Son installation est un "aller-simple" : une fois les hashs des auteurs changés avec les nouvelles fonctions il n'est pas possible de revenir en arrière en désactivant le plugin.

Si le plugin est désactivé : il n'est plus possible de s'authentifier avec un auteur dont le hash du mot de passe a été MAJ. Il faudra dans ce cas fixer un nouveau mot de passe pour cet auteur.

Ce plugin a vocation a être intégré dans plugins-dist/ ou dans le core dans une 4.x future.


### Intégration pour sécurité+++ ###

Afin de limiter les impacts d'un accès non autorisé en lecture a la base de données, il reste nécessaire de protéger (chiffrer) les données permettant de s'authentifier ou équivalent. Quelques exemples :

- champ cooki_oubli contenant le token envoyé par email a l'utilisateur lors de la perte du mot de passe
- champ secret_du_site/alea* contenu dans la table spip_meta
- etc ?

Le plugin "chiffrer" n'a pas vocation a bloquer tous ces "chemins". Ce travail doit amha être fait au fil de l'eau dans SPIP et peut être réalisé simplement à l'aide des fonctions chiffrer()/dechiffrer().

Utiliser le plugin en l'état, ou l'intégrer dès maintenant au core ou à la dist (https://git.spip.net/spip/spip/issues/4927).



