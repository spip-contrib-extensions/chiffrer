<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Chiffrer
 *
 * @plugin     Chiffrer
 * @copyright  2021
 * @author     g0uZ
 * @licence    GNU/GPL
 * @package    SPIP\Chiffrer\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Installation de la clé de chiffrement
 */
function chiffrement_installation(){
	include_spip("chiffrer_fonctions");

	initialiser_cle();
}
	
/**
 * Suppression de la clé de chiffrement
 */	
function chiffrement_desinstallation(){
	include_spip("chiffrer_fonctions");

}

/**
 * Fonction d'installation et de mise à jour du plugin Chiffrer.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function chiffrer_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

// 	$maj['create'] = array(
// 		array('chiffrement_installation'),
// 	);
// 	
	chiffrement_installation();
	
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);	
}

/**
 * Fonction de désinstallation du plugin Chiffrer.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function chiffrer_vider_tables($nom_meta_base_version) {
	chiffrement_desinstallation();
	effacer_meta($nom_meta_base_version);
}
