<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chiffrer_description' => 'Ajoute un mécanisme de chiffrement/déchiffrement à Spip.',
	'chiffrer_nom' => 'Chiffrer',
	'chiffrer_slogan' => 'Protéger vos données en les chiffrant !',
);
